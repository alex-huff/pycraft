package com.alexfh.pycraft.command;

import com.alexfh.pycraft.record.RecordingException;
import com.alexfh.pycraft.record.RecordingManager;
import com.alexfh.pycraft.script.ScriptManager;
import com.alexfh.pycraft.util.PycraftUtil;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;

public
class CommandHandler
{

    public static CommandHandler INSTANCE = new CommandHandler();

    private
    String[] truncateArguments(String[] arguments, int startIndex)
    {
        String[] newArguments = new String[arguments.length - startIndex];
        System.arraycopy(arguments, startIndex, newArguments, 0, newArguments.length);
        return newArguments;
    }

    private
    void verifyNumArguments(String[] arguments, int expectedNumArguments) throws CommandException
    {
        this.verifyNumArguments(arguments, expectedNumArguments, expectedNumArguments);
    }

    private
    void verifyNumArguments(String[] arguments, int min, int max) throws CommandException
    {
        int numArguments = arguments.length;
        if (numArguments < min)
        {
            throw new CommandException("this command expects at least " + min + " arguments");
        }
        else if (numArguments > max)
        {
            throw new CommandException("this command expects at most " + max + " arguments");
        }
    }

    private
    Path getScriptPath(String relativePath) throws CommandException
    {
        Path scriptPath;
        try
        {
            scriptPath = PycraftUtil.getScriptsDirectory().resolve(relativePath);
        }
        catch (InvalidPathException invalidPathException)
        {
            throw new CommandException("invalid script path: " + relativePath);
        }
        return scriptPath;
    }


    private
    void handleRecordSave(String[] arguments) throws CommandException, RecordingException
    {
        this.verifyNumArguments(arguments, 1);
        String                     recordingName       = arguments[0];
        CompletableFuture<Boolean> saveRecordingFuture = RecordingManager.INSTANCE.saveRecording(recordingName);
        saveRecordingFuture.whenComplete((ignored, error) ->
        {
            if (error != null)
            {
                error.printStackTrace();
            }
        });
    }

    private
    void handleRun(String[] arguments) throws CommandException
    {
        this.verifyNumArguments(arguments, 1, Integer.MAX_VALUE);
        String   relativePath    = arguments[0];
        Path     scriptPath      = this.getScriptPath(relativePath);
        String[] scriptArguments = this.truncateArguments(arguments, 1);
        ScriptManager.INSTANCE.runScript(scriptPath, scriptArguments);
    }

    private
    void handleRecord(String[] arguments) throws CommandException, RecordingException
    {
        this.verifyNumArguments(arguments, 1, Integer.MAX_VALUE);
        String[] subcommandArguments = this.truncateArguments(arguments, 1);
        switch (arguments[0])
        {
            case "start" -> RecordingManager.INSTANCE.startRecording();
            case "stop" -> RecordingManager.INSTANCE.stopRecording();
            case "save" -> this.handleRecordSave(subcommandArguments);
        }
    }

    private
    void handlePlaybackStart(String[] arguments) throws CommandException, RecordingException
    {
        this.verifyNumArguments(arguments, 0, 1);
        if (arguments.length == 0)
        {
            RecordingManager.INSTANCE.playbackRecording((ignored) ->
            {
            });
            return;
        }
        String                     recordingName  = arguments[0];
        CompletableFuture<Boolean> playbackFuture = RecordingManager.INSTANCE.playbackRecording(recordingName);
        playbackFuture.whenComplete((ignored, error) ->
        {
            if (error != null)
            {
                error.printStackTrace();
            }
        });
    }

    private
    void handlePlayback(String[] arguments) throws CommandException, RecordingException
    {
        this.verifyNumArguments(arguments, 1, Integer.MAX_VALUE);
        String[] subcommandArguments = this.truncateArguments(arguments, 1);
        switch (arguments[0])
        {
            case "start" -> this.handlePlaybackStart(subcommandArguments);
            case "stop" -> RecordingManager.INSTANCE.cancelPlayback();
        }
    }


    public
    void onCommand(String message)
    {
        String[] arguments = message.split("\\s+");
        if (!arguments[0].equals(".pycraft") || arguments.length < 2)
        {
            return;
        }
        String[] subcommandArguments = this.truncateArguments(arguments, 2);
        try
        {
            switch (arguments[1])
            {
                case "run" -> this.handleRun(subcommandArguments);
                case "record" -> this.handleRecord(subcommandArguments);
                case "playback" -> this.handlePlayback(subcommandArguments);
                default ->
                {
                }
            }
        }
        catch (RecordingException recordingException)
        {
            recordingException.printStackTrace();
        }
        catch (CommandException commandException)
        {
            commandException.printStackTrace();
        }
    }
}
