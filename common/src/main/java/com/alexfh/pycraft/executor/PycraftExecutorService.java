package com.alexfh.pycraft.executor;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public
class PycraftExecutorService extends ThreadPoolExecutor
{
    public static final PycraftExecutorService INSTANCE
        = new PycraftExecutorService(0, Integer.MAX_VALUE, 60L, TimeUnit.SECONDS, new SynchronousQueue<>());

    public
    PycraftExecutorService(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit timeUnit,
                           BlockingQueue<Runnable> workQueue)
    {
        super(corePoolSize, maximumPoolSize, keepAliveTime, timeUnit, workQueue);
    }
}
