package com.alexfh.pycraft;

import com.alexfh.pycraft.command.CommandHandler;
import com.alexfh.pycraft.util.PycraftUtil;
import dev.architectury.event.EventResult;
import dev.architectury.event.events.client.ClientChatEvent;
import jep.JepConfig;
import jep.MainInterpreter;
import jep.SharedInterpreter;

public
class Pycraft
{
    public static final String MOD_ID = "pycraft";

    public static
    void init()
    {
        PycraftUtil.verifyPycraftDirectoriesExist();
        MainInterpreter.setJepLibraryPath(System.getenv("JEP_LIB_PATH"));
        SharedInterpreter.setConfig(new JepConfig().redirectStdout(System.out).redirectStdErr(System.err));
        ClientChatEvent.SEND.register((message, component) ->
        {
            if (message.startsWith(".pycraft"))
            {
                CommandHandler.INSTANCE.onCommand(message);
                return EventResult.interruptFalse();
            }
            return EventResult.pass();
        });
    }
}
