package com.alexfh.pycraft.record;

public
class RecordingException extends Exception
{
    public
    RecordingException(String message)
    {
        super(message);
    }
}
