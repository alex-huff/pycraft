package com.alexfh.pycraft.record;

import com.alexfh.pycraft.executor.PycraftExecutorService;
import com.alexfh.pycraft.input.InputManager;
import com.alexfh.pycraft.input.action.InputAction;
import com.alexfh.pycraft.input.action.impl.CursorPosInputAction;
import com.alexfh.pycraft.input.action.impl.KeyInputAction;
import com.alexfh.pycraft.input.action.impl.NOOPInputAction;
import com.alexfh.pycraft.io.format.pcr.PCRException;
import com.alexfh.pycraft.io.format.pcr.PCRFormat;
import com.alexfh.pycraft.phase.Phase;
import com.alexfh.pycraft.util.PycraftUtil;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.Mouse;
import org.lwjgl.glfw.GLFW;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.function.Supplier;

public
class RecordingManager
{
    public static final RecordingManager  INSTANCE                  = new RecordingManager();
    private final       MinecraftClient   minecraftClient           = MinecraftClient.getInstance();
    private final       Mouse             mouse                     = this.minecraftClient.mouse;
    private             int               recordingCurrentTick      = 0;
    private             boolean           recordingInProgress       = false;
    private             boolean           playbackInProgress        = false;
    private             Recording         currentRecording          = null;
    private             Player            player                    = null;
    private             Consumer<Boolean> playbackCompletedCallback = null;


    public
    void startRecording() throws RecordingException
    {
        if (this.recordingInProgress)
        {
            throw new RecordingException("recording is already in progress");
        }
        this.currentRecording     = new Recording(this.minecraftClient);
        this.recordingCurrentTick = 0;
        this.recordingInProgress  = true;
    }

    public
    void stopRecording() throws RecordingException
    {
        if (!this.recordingInProgress)
        {
            throw new RecordingException("not currently recording");
        }
        this.addActionToRecording(new NOOPInputAction());
        this.recordingInProgress = false;
    }

    public
    CompletableFuture<Boolean> saveRecording(String recordingName) throws RecordingException
    {
        if (this.recordingInProgress)
        {
            this.stopRecording();
        }
        Recording recordingToSave = this.currentRecording;
        if (recordingToSave == null)
        {
            throw new RecordingException("no recording to save");
        }
        Path filePath = PycraftUtil.getRecordingDirectory().resolve(recordingName + PCRFormat.EXTENSION);
        CompletableFuture<Boolean> completableFuture = new CompletableFuture<>();
        PycraftExecutorService.INSTANCE.submit(() ->
        {
            try
            {
                PCRFormat.writePCRFile(filePath, recordingToSave, true);
            }
            catch (Throwable t)
            {
                this.minecraftClient.execute(() -> completableFuture.completeExceptionally(t));
            }
            this.minecraftClient.execute(() -> completableFuture.complete(true));
        });
        return completableFuture;
    }

    public
    void playbackRecording(Consumer<Boolean> onComplete) throws RecordingException
    {
        if (this.currentRecording == null)
        {
            throw new RecordingException("no recording to playback");
        }
        this.playbackRecording(this.currentRecording, onComplete);
    }

    public
    CompletableFuture<Boolean> playbackRecording(String recordingName)
    {
        CompletableFuture<Boolean> playbackComplete = new CompletableFuture<>();
        PycraftExecutorService.INSTANCE.submit(() ->
        {
            try
            {
                Recording recording = PCRFormat.readPCRFile(PycraftUtil.getRecordingDirectory()
                    .resolve(recordingName + PCRFormat.EXTENSION));
                this.minecraftClient.execute(() ->
                {
                    try
                    {
                        this.playbackRecording(recording, playbackComplete::complete);
                    }
                    catch (RecordingException recordingException)
                    {
                        playbackComplete.completeExceptionally(recordingException);
                    }
                });
            }
            catch (IOException | PCRException e)
            {
                this.minecraftClient.execute(() -> playbackComplete.completeExceptionally(e));
            }
        });
        return playbackComplete;
    }

    public
    void playbackRecording(Recording recording, Consumer<Boolean> onComplete) throws RecordingException
    {
        if (this.playbackInProgress)
        {
            throw new RecordingException("playback is already in progress");
        }
        if (this.recordingInProgress)
        {
            throw new RecordingException("Cannot playback current recording. Recording is still in progress.");
        }
        this.player                    = new Player(recording);
        this.playbackCompletedCallback = onComplete;
        this.playbackInProgress        = true;
    }

    public
    void cancelPlayback() throws RecordingException
    {
        if (!this.playbackInProgress)
        {
            throw new RecordingException("no playback to cancel");
        }
        this.terminatePlayback(false);
    }

    private
    void terminatePlayback(boolean successful)
    {
        Consumer<Boolean> completedCallback = this.playbackCompletedCallback;
        this.playbackCompletedCallback = null;
        this.player                    = null;
        this.playbackInProgress        = false;
        GLFW.glfwSetCursorPos(this.minecraftClient.getWindow().getHandle(), this.mouse.x, this.mouse.y);
        InputManager.INSTANCE.releaseAllInput();
        completedCallback.accept(successful);
    }

    private
    void processPlayback(Phase phase)
    {
        if (this.playbackInProgress)
        {
            Supplier<Boolean> playbackProcessor;
            if (phase == Phase.TICK_HEAD)
            {
                playbackProcessor = this.player::onTick;
            }
            else if (phase == Phase.RENDER_HEAD)
            {
                playbackProcessor = this.player::onRender;
            }
            else
            {
                throw new IllegalArgumentException("invalid playback phase to process");
            }
            boolean completed = playbackProcessor.get();
            if (completed)
            {
                boolean playbackFailed = this.player.getPlaybackFailed();
                this.terminatePlayback(!playbackFailed);
            }
        }
    }

    public
    void onTick()
    {
        if (this.recordingInProgress)
        {
            this.recordingCurrentTick++;
        }
        this.processPlayback(Phase.TICK_HEAD);
    }

    public
    void onRender()
    {
        this.processPlayback(Phase.RENDER_HEAD);
    }

    private
    void addActionToRecording(InputAction inputAction)
    {
        this.currentRecording.addAction(inputAction, this.recordingCurrentTick, this.minecraftClient.getTickDelta());
    }

    public
    boolean onInput(boolean simulated, InputAction inputAction)
    {
        if (!simulated)
        {
            if (this.recordingInProgress)
            {
                this.addActionToRecording(inputAction);
            }
            if (this.playbackInProgress)
            {
                boolean isCursorMove = inputAction instanceof CursorPosInputAction;
                if (isCursorMove)
                {
                    return true;
                }
                boolean isEnterRelease = false;
                if (inputAction instanceof KeyInputAction keyInputAction)
                {
                    boolean isEnter   = keyInputAction.key == GLFW.GLFW_KEY_ENTER;
                    boolean isRelease = keyInputAction.action == GLFW.GLFW_RELEASE;
                    if (isEnter && isRelease)
                    {
                        isEnterRelease = true;
                    }
                }
                if (isEnterRelease)
                {
                    return true;
                }
                this.terminatePlayback(false);
            }
        }
        return false;
    }
}