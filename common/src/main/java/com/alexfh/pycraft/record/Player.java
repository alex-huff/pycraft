package com.alexfh.pycraft.record;

import com.alexfh.pycraft.util.MinecraftUtil;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import org.lwjgl.glfw.GLFW;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.function.Predicate;

public
class Player
{
    private final MinecraftClient             minecraftClient = MinecraftClient.getInstance();
    private final Recording                   recording;
    private final Queue<RecordingInputAction> inputActionQueue;
    private       int                         playbackTick    = 0;
    private       boolean                     playbackStarted = false;
    private       String                      errorMessage    = null;
    private       boolean                     playbackFailed  = false;

    public
    Player(Recording recording)
    {
        this.recording        = recording;
        this.inputActionQueue = new ArrayDeque<>(recording.actions);
    }

    private
    boolean isCompleted()
    {
        return this.inputActionQueue.isEmpty();
    }

    private
    boolean processInputActionsWhile(Predicate<RecordingInputAction> condition)
    {
        while (!this.isCompleted() && condition.test(this.inputActionQueue.peek()))
        {
            this.inputActionQueue.remove().simulateAction();
        }
        return this.isCompleted();
    }

    private
    void setErrorMessage(String errorMessage)
    {
        this.errorMessage   = errorMessage;
        this.playbackFailed = true;
    }

    public
    boolean getPlaybackFailed()
    {
        return this.playbackFailed;
    }

    public
    String getErrorMessage()
    {
        return this.errorMessage;
    }

    public
    boolean onTick()
    {
        if (!this.playbackStarted)
        {
            this.playbackStarted = true;
            if (this.recording.playerPresent)
            {
                ClientPlayerEntity player = this.minecraftClient.player;
                if (player == null)
                {
                    this.setErrorMessage("recording had player, but no player was present");
                    return true;
                }
                MinecraftUtil.lookAt(player, this.recording.startPitch, this.recording.startYaw, true);
            }
            GLFW.glfwSetCursorPos(this.minecraftClient.getWindow()
                .getHandle(), this.recording.startMouseX, this.recording.startMouseY);
            this.minecraftClient.mouse.x = this.recording.startMouseX;
            this.minecraftClient.mouse.y = this.recording.startMouseY;
            return this.isCompleted();
        }
        this.playbackTick++;
        return this.processInputActionsWhile(action -> action.tick < this.playbackTick);
    }

    public
    boolean onRender()
    {
        if (!this.playbackStarted)
        {
            return this.isCompleted();
        }
        return this.processInputActionsWhile(action -> action.tick <= this.playbackTick &&
                                                       action.tickDelta < this.minecraftClient.getTickDelta());
    }
}
