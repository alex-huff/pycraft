package com.alexfh.pycraft.record;

import com.alexfh.pycraft.input.action.InputAction;
import com.alexfh.pycraft.input.action.impl.*;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.util.List;

public
class Compiler
{

    @FunctionalInterface
    private
    interface WriterFunction
    {
        void write(Writer writer) throws IOException;
    }

    public static
    void compileRecordingToScript(Writer writer, Recording recording) throws IOException
    {
        boolean hasActions                    = !recording.actions.isEmpty();
        Package recordingPackage              = Recording.class.getPackage();
        String  recordingPackageName          = recordingPackage.getName();
        String  recordingClassName            = Recording.class.getSimpleName();
        String  recordingInputActionClassName = RecordingInputAction.class.getSimpleName();
        Compiler.compileImportStatement(writer, recordingPackageName, recordingClassName, recordingInputActionClassName);
        writer.append("\n");
        if (hasActions)
        {
            Package inputActionsPackage     = recording.actions.get(0).inputAction.getClass().getPackage();
            String  inputActionsPackageName = inputActionsPackage.getName();
            Compiler.compileImportStatement(writer, inputActionsPackageName, "*");
            writer.append("\n");
        }
        writer.append("recording = ");
        Compiler.compileRecordingInitialization(writer, recording);
        writer.append("\n");
        for (RecordingInputAction recordingInputAction : recording.actions)
        {
            writer.append("recording.addAction(");
            Compiler.compileAddActionArguments(writer, recordingInputAction);
            writer.append(")");
            writer.append("\n");
        }
        writer.append("controller.yieldTill('tick')");
        writer.append("\n");
        writer.append("mc.setSimulateInputs(True)");
        writer.append("\n");
        writer.append("mc.turnHead(");
        Compiler.compileCommaSeparatedValues(writer, 10, recording.startPitch, recording.startYaw);
        writer.append(")");
        writer.append("\n");
        writer.append("success = mc.playbackRecording(recording)");
        writer.append("\n");
        writer.append("print('Playback succeeded' if success else 'Playback failed')");
    }

    private static
    void write(Writer writer, Object object) throws IOException
    {
        if (object instanceof WriterFunction writerFunction)
        {
            writerFunction.write(writer);
        }
        else
        {
            writer.append(object.toString());
        }
    }

    private static
    void compileCommaSeparatedValues(Writer writer, Object... values) throws IOException
    {
        if (values.length == 0)
        {
            return;
        }
        Compiler.write(writer, values[0]);
        for (int i = 1; i < values.length; i++)
        {
            writer.append(", ");
            Compiler.write(writer, values[i]);
        }
    }

    private static
    void compileImportStatement(Writer writer, String packageName, String... toImport) throws IOException
    {
        if (toImport.length == 0)
        {
            return;
        }
        writer.append("from ").append(packageName).append(" import ");
        Compiler.compileCommaSeparatedValues(writer, (Object[]) toImport);
    }

    // @formatter:off
    private static
    void compileRecordingInitialization(Writer writer, Recording recording) throws IOException
    {
        writer.append(Recording.class.getSimpleName()).append("(");
        Compiler.compileCommaSeparatedValues(
            writer,
            recording.startMouseX,
            recording.startMouseY,
            (WriterFunction) w -> Compiler.compileBoolean(w, recording.playerPresent),
            recording.startPitch,
            recording.startYaw
        );
        writer.append(")");
    }
    // @formatter:on

    // @formatter:off
    private static
    void compileAddActionArguments(Writer writer, RecordingInputAction recordingInputAction) throws IOException
    {
        Compiler.compileCommaSeparatedValues(
            writer,
            (WriterFunction) w -> Compiler.compileInputAction(w, recordingInputAction.inputAction),
            recordingInputAction.tick,
            recordingInputAction.tickDelta
        );
    }
    // @formatter:on

    // @formatter:off
    private static
    void compileInputAction(Writer writer, InputAction inputAction) throws IOException
    {
        writer.append(inputAction.getClass().getSimpleName()).append("(");
        if (inputAction instanceof CharInputAction charInputAction)
        {
            Compiler.compileCommaSeparatedValues(
                writer,
                charInputAction.codepoint,
                charInputAction.modifiers
            );
        }
        else if (inputAction instanceof CursorPosInputAction cursorPosInputAction)
        {
            Compiler.compileCommaSeparatedValues(
                writer,
                cursorPosInputAction.x,
                cursorPosInputAction.y
            );
        }
        else if (inputAction instanceof FilesDroppedInputAction filesDroppedInputAction)
        {
            Compiler.compileCommaSeparatedValues(
                writer,
                (WriterFunction) w -> Compiler.compilePathList(w, filesDroppedInputAction.paths)
            );
        }
        else if (inputAction instanceof KeyInputAction keyInputAction)
        {
            Compiler.compileCommaSeparatedValues(
                writer,
                keyInputAction.key,
                keyInputAction.scancode,
                keyInputAction.action,
                keyInputAction.modifiers
            );
        }
        else if (inputAction instanceof MouseButtonInputAction mouseButtonInputAction)
        {
            Compiler.compileCommaSeparatedValues(
                writer,
                mouseButtonInputAction.button,
                mouseButtonInputAction.action,
                mouseButtonInputAction.mods
            );
        }
        else if (inputAction instanceof MouseScrollInputAction mouseScrollInputAction)
        {
            Compiler.compileCommaSeparatedValues(
                writer,
                mouseScrollInputAction.horizontal,
                mouseScrollInputAction.vertical
            );
        }
        writer.append(")");
    }
    // @formatter:on

    private static
    void compileBoolean(Writer writer, boolean value) throws IOException
    {
        writer.append(value ? "True" : "False");
    }

    // @formatter:off
    private static
    void compilePathList(Writer writer, List<Path> paths) throws IOException
    {
        writer.append("[");
        Compiler.compileCommaSeparatedValues(
            writer,
            paths.stream().map(Path::toString).toArray()
        );
        writer.append("]");
    }
    // @formatter:on
}
