package com.alexfh.pycraft.record;

import com.alexfh.pycraft.input.action.InputAction;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public
class RecordingInputAction
{
    public final InputAction inputAction;
    public final int         tick;
    public final float       tickDelta;

    public static
    RecordingInputAction deserialize(DataInputStream dataInputStream) throws IOException
    {
        return new RecordingInputAction(InputAction.deserialize(dataInputStream), dataInputStream.readInt(), dataInputStream.readFloat());
    }

    public
    RecordingInputAction(InputAction inputAction, int tick, float tickDelta)
    {
        this.inputAction = inputAction;
        this.tick        = tick;
        this.tickDelta   = tickDelta;
    }

    public
    void simulateAction()
    {
        this.inputAction.simulateAction();
    }

    public
    void serialize(DataOutputStream dataOutputStream) throws IOException
    {
        this.inputAction.serialize(dataOutputStream);
        dataOutputStream.writeInt(this.tick);
        dataOutputStream.writeFloat(this.tickDelta);
    }
}
