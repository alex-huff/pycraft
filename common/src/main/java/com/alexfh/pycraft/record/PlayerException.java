package com.alexfh.pycraft.record;

public
class PlayerException extends Exception
{
    public
    PlayerException(String message)
    {
        super(message);
    }
}
