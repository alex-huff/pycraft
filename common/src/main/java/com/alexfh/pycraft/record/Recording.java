package com.alexfh.pycraft.record;

import com.alexfh.pycraft.input.action.InputAction;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.Mouse;
import net.minecraft.client.network.ClientPlayerEntity;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public
class Recording
{
    public final List<RecordingInputAction> actions;
    public final float                      startPitch;
    public final float                      startYaw;
    public final double                     startMouseX;
    public final double                     startMouseY;
    public final boolean                    playerPresent;

    public static
    Recording deserialize(DataInputStream dataInputStream) throws IOException
    {
        double                     startMouseX           = dataInputStream.readDouble();
        double                     startMouseY           = dataInputStream.readDouble();
        boolean                    playerPresent         = dataInputStream.readBoolean();
        float                      startPitch            = dataInputStream.readFloat();
        float                      startYaw              = dataInputStream.readFloat();
        int                        numActions            = dataInputStream.readInt();
        List<RecordingInputAction> recordingInputActions = new ArrayList<>(numActions);
        for (int i = 0; i < numActions; i++)
        {
            recordingInputActions.add(RecordingInputAction.deserialize(dataInputStream));
        }
        return new Recording(startMouseX, startMouseY, playerPresent, startPitch, startYaw, recordingInputActions);
    }

    private
    Recording(double startMouseX, double startMouseY, boolean playerPresent, float startPitch, float startYaw,
              List<RecordingInputAction> actions)
    {
        this.startMouseX   = startMouseX;
        this.startMouseY   = startMouseY;
        this.playerPresent = playerPresent;
        this.startPitch    = startPitch;
        this.startYaw      = startYaw;
        this.actions       = actions;
    }

    public
    Recording(double startMouseX, double startMouseY, boolean playerPresent, float startPitch, float startYaw)
    {
        this(startMouseX, startMouseY, playerPresent, startPitch, startYaw, new ArrayList<>());
    }

    public
    Recording(double startMouseX, double startMouseY)
    {
        this(startMouseX, startMouseY, false, 0, 0);
    }

    public
    Recording(MinecraftClient minecraftClient)
    {
        Mouse              mouse  = minecraftClient.mouse;
        ClientPlayerEntity player = minecraftClient.player;
        this.startMouseX   = mouse.x;
        this.startMouseY   = mouse.y;
        this.playerPresent = player != null;
        if (this.playerPresent)
        {
            this.startPitch = player.getPitch();
            this.startYaw   = player.getYaw();
        }
        else
        {
            this.startPitch = 0;
            this.startYaw   = 0;
        }
        this.actions = new ArrayList<>();
    }

    public
    void addAction(InputAction inputAction, int tick, float tickDelta)
    {
        this.actions.add(new RecordingInputAction(inputAction, tick, tickDelta));
    }

    public
    void serialize(DataOutputStream dataOutputStream) throws IOException
    {
        dataOutputStream.writeDouble(this.startMouseX);
        dataOutputStream.writeDouble(this.startMouseY);
        dataOutputStream.writeBoolean(this.playerPresent);
        dataOutputStream.writeFloat(this.startPitch);
        dataOutputStream.writeFloat(this.startYaw);
        dataOutputStream.writeInt(this.actions.size());
        for (RecordingInputAction inputAction : this.actions)
        {
            inputAction.serialize(dataOutputStream);
        }
    }

}
