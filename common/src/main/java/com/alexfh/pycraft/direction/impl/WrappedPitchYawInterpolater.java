package com.alexfh.pycraft.direction.impl;

import com.alexfh.pycraft.direction.PitchYawInterpolater;
import com.alexfh.pycraft.direction.YawDirection;
import com.alexfh.pycraft.util.MinecraftUtil;
import net.minecraft.util.math.MathHelper;

public
class WrappedPitchYawInterpolater extends PitchYawInterpolater
{
    private final float startPitch;
    private final float startYaw;
    private final float endPitch;
    private final float endYaw;
    private final float unwrappedEndYaw;

    public
    WrappedPitchYawInterpolater(int totalTicks, float startPitch, float startYaw, float endPitch, float endYaw,
                                YawDirection yawDirection)
    {
        super(totalTicks);
        this.startPitch = MinecraftUtil.clampPitch(startPitch);
        this.startYaw   = MathHelper.wrapDegrees(startYaw);
        this.endPitch   = MinecraftUtil.clampPitch(endPitch);
        this.endYaw     = MathHelper.wrapDegrees(endYaw);
        float   distance          = Math.abs(this.endYaw - this.startYaw);
        float   alternateDistance = 360 - distance;
        boolean positive;
        switch (yawDirection)
        {
            case CLOSEST -> positive = distance < 180 == this.endYaw > this.startYaw;
            case POSITIVE -> positive = true;
            case NEGATIVE -> positive = false;
            default -> throw new RuntimeException("unknown YawDirection");
        }
        if (this.startYaw < this.endYaw != positive)
        {
            this.unwrappedEndYaw = this.startYaw + (positive ? 1 : -1) * alternateDistance;
        }
        else
        {
            this.unwrappedEndYaw = this.endYaw;
        }
    }

    @Override
    public
    void apply(double percent)
    {
        this.pitch = this.interpolate(this.startPitch, this.endPitch, percent);
        if (percent == 1)
        {
            // avoid floating point error on final yaw position
            this.yaw = this.endYaw;
        }
        else
        {
            this.yaw = MathHelper.wrapDegrees(this.interpolate(this.startYaw, this.unwrappedEndYaw, percent));
        }
    }
}
