package com.alexfh.pycraft.direction;

public
enum YawDirection
{
    POSITIVE, NEGATIVE, CLOSEST;
}
