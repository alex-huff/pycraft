package com.alexfh.pycraft.direction;

@FunctionalInterface
public
interface Interpolater
{
    double interpolate(double start, double finish, double percent);
}
