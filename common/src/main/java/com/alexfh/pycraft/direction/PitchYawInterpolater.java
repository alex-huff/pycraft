package com.alexfh.pycraft.direction;

import com.alexfh.pycraft.util.MathUtil;

public abstract
class PitchYawInterpolater
{
    protected       Interpolater interpolater = MathUtil::lerp;
    protected final int          totalTicks;
    protected       float        pitch;
    protected       float        yaw;

    protected
    PitchYawInterpolater(int totalTicks)
    {
        this.totalTicks = totalTicks;
    }

    public
    int getTotalTicks()
    {
        return this.totalTicks;
    }

    public
    float getPitch()
    {
        return this.pitch;
    }

    public
    float getYaw()
    {
        return this.yaw;
    }

    protected
    float interpolate(double start, double finish, double percent)
    {
        return (float) this.interpolater.interpolate(start, finish, percent);
    }

    public
    void setInterpolater(Interpolater interpolater)
    {
        this.interpolater = interpolater;
    }

    public
    void apply(int tick, float tickDelta)
    {
        double percent = this.totalTicks == 0 ? 1 : ((tick + tickDelta) / this.totalTicks);
        this.apply(percent);
    }

    protected abstract
    void apply(double percent);
}
