package com.alexfh.pycraft.mixin;

import com.alexfh.pycraft.ExpectPlatform;
import jep.ClassList;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import java.nio.file.Path;
import java.util.StringTokenizer;

@Mixin(ClassList.class)
public
class MixinClassList
{
    @Redirect(
        method = "loadClassPath()V", at = @At(
        value = "NEW", target = "(Ljava/lang/String;Ljava/lang/String;)Ljava/util/StringTokenizer;"))
    public
    StringTokenizer constructTokenizer(String str, String delim)
    {
        return new StringTokenizer(String.join(delim, ExpectPlatform.getClassPath().stream().map(Path::toString)
            .toList()), delim);
    }
}
