package com.alexfh.pycraft.mixin;

import com.alexfh.pycraft.input.InputManager;
import com.alexfh.pycraft.input.PycraftKeyboard;
import com.alexfh.pycraft.input.PycraftMouse;
import com.alexfh.pycraft.phase.Phase;
import com.alexfh.pycraft.record.RecordingManager;
import com.alexfh.pycraft.script.ScriptManager;
import net.minecraft.client.Keyboard;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.Mouse;
import net.minecraft.client.util.InputUtil;
import org.lwjgl.glfw.GLFWDropCallback;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.IntStream;

@Mixin(MinecraftClient.class)
public abstract
class MixinMinecraftClient
{
    // @formatter:off
    @Redirect(method = "<init>", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/Mouse;setup(J)V"))
    public
    void mouseSetup(Mouse mouse, long handle)
    {
        InputManager.WINDOW_HANDLE = handle;
        PycraftMouse.INSTANCE.setMouse(mouse);
        MinecraftClient minecraftClient = (MinecraftClient) (Object) this;
        PycraftMouse    pycraftMouse    = PycraftMouse.INSTANCE;
        InputUtil.setMouseCallbacks(
            handle,
            (window, x, y) ->
                minecraftClient.execute(() -> pycraftMouse.onCursorPos(false, x, y)),
            (window, button, action, mods) ->
                minecraftClient.execute(() -> pycraftMouse.onMouseButton(false, button, action, mods)),
            (window, horizontal, vertical) ->
                minecraftClient.execute(() -> pycraftMouse.onMouseScroll(false, horizontal, vertical)),
            (window, count, names) ->
            {
                List<Path> paths = IntStream.range(0, count).mapToObj(i -> Paths.get(GLFWDropCallback.getName(names, i))).toList();
                minecraftClient.execute(() -> pycraftMouse.onFilesDropped(false, paths));
            }
        );
    }
    // @formatter:on

    // @formatter:off
    @Redirect(method = "<init>", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/Keyboard;setup(J)V"))
    public
    void keyboardSetup(Keyboard keyboard, long handle)
    {
        PycraftKeyboard.INSTANCE.setKeyboard(keyboard);
        MinecraftClient minecraftClient = (MinecraftClient) (Object) this;
        PycraftKeyboard pycraftKeyboard = PycraftKeyboard.INSTANCE;
        InputUtil.setKeyboardCallbacks(
            handle,
            (window, key, scancode, action, modifiers) ->
                minecraftClient.execute(() -> pycraftKeyboard.onKey(false, key, scancode, action, modifiers)),
            (window, codepoint, modifiers) ->
                minecraftClient.execute(() -> pycraftKeyboard.onChar(false, codepoint, modifiers))
        );
    }
    // @formatter:on

    @Inject(at = @At("HEAD"), method = "tick()V")
    private
    void preTick(CallbackInfo info)
    {
        RecordingManager.INSTANCE.onTick();
        ScriptManager.INSTANCE.continueScriptsWaitingForPhase(Phase.TICK_HEAD);
    }

    @Inject(at = @At("HEAD"), method = "render(Z)V")
    private
    void preRender(CallbackInfo info)
    {
        RecordingManager.INSTANCE.onRender();
        ScriptManager.INSTANCE.continueScriptsWaitingForPhase(Phase.RENDER_HEAD);
    }
}
