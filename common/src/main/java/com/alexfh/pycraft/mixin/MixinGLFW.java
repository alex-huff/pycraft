package com.alexfh.pycraft.mixin;

import com.alexfh.pycraft.input.InputManager;
import org.lwjgl.glfw.GLFW;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(GLFW.class)
public
class MixinGLFW
{
    @Inject(method = "glfwGetKey(JI)I", at = @At("HEAD"), cancellable = true, remap = false)
    private static
    void isKeyPressed(long window, int key, CallbackInfoReturnable<Integer> cir)
    {
        if (window != InputManager.WINDOW_HANDLE)
        {
            return;
        }
        cir.setReturnValue(InputManager.INSTANCE.isKeyPressed(key) ? GLFW.GLFW_PRESS : GLFW.GLFW_RELEASE);
    }
}
