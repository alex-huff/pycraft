package com.alexfh.pycraft.io.format.pcr;

import com.alexfh.pycraft.record.Recording;

import java.io.*;
import java.nio.file.Path;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public
class PCRFormat
{
    public static final byte   VERSION   = 0;
    public static final String EXTENSION = ".pcr";

    public static
    void writePCRFile(Path filePath, Recording recording, boolean compressed) throws IOException
    {
        File             pcrFile          = filePath.toFile();
        FileOutputStream fileOutputStream = new FileOutputStream(pcrFile);
        try (fileOutputStream)
        {
            fileOutputStream.write(VERSION);
            fileOutputStream.write(compressed ? 1 : 0);
            OutputStream underlyingStream = compressed ? new GZIPOutputStream(fileOutputStream) : fileOutputStream;
            try (DataOutputStream dataOutputStream = new DataOutputStream(underlyingStream))
            {
                recording.serialize(dataOutputStream);
            }
        }
    }

    public static
    Recording readPCRFile(Path filePath) throws IOException, PCRException
    {
        File            pcrFile         = filePath.toFile();
        FileInputStream fileInputStream = new FileInputStream(pcrFile);
        try (fileInputStream)
        {
            byte version = (byte) fileInputStream.read();
            if (version != PCRFormat.VERSION)
            {
                throw new PCRException("unsupported pcr version: " + version);
            }
            boolean     compressed       = fileInputStream.read() == 1;
            InputStream underlyingStream = compressed ? new GZIPInputStream(fileInputStream) : fileInputStream;
            try (DataInputStream dataInputStream = new DataInputStream(underlyingStream))
            {
                return Recording.deserialize(dataInputStream);
            }
        }
    }
}
