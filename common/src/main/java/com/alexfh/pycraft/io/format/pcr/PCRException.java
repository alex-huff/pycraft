package com.alexfh.pycraft.io.format.pcr;

public
class PCRException extends Exception
{
    public
    PCRException(String message)
    {
        super(message);
    }
}
