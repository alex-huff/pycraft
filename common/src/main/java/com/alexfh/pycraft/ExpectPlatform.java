package com.alexfh.pycraft;

import java.nio.file.Path;
import java.util.List;

public
class ExpectPlatform
{
    @dev.architectury.injectables.annotations.ExpectPlatform
    public static
    Path getGameDirectory()
    {
        throw new AssertionError();
    }

    @dev.architectury.injectables.annotations.ExpectPlatform
    public static
    List<Path> getClassPath()
    {
        throw new AssertionError();
    }
}
