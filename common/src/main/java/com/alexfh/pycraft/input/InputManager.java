package com.alexfh.pycraft.input;

public
class InputManager
{
    public static final InputManager    INSTANCE = new InputManager();
    public static       long            WINDOW_HANDLE;
    private final       PycraftKeyboard keyboard = PycraftKeyboard.INSTANCE;
    private final       PycraftMouse    mouse    = PycraftMouse.INSTANCE;

    public
    void releaseAllInput()
    {
        this.keyboard.releaseAllInput();
        this.mouse.releaseAllInput();
    }

    public
    boolean isKeyPressed(int key)
    {
        return this.keyboard.getPressed(key);
    }
}
