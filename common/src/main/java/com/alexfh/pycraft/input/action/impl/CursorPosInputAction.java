package com.alexfh.pycraft.input.action.impl;

import com.alexfh.pycraft.input.InputManager;
import com.alexfh.pycraft.input.PycraftMouse;
import com.alexfh.pycraft.input.action.InputAction;
import com.alexfh.pycraft.input.action.InputActionType;
import org.lwjgl.glfw.GLFW;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public
class CursorPosInputAction extends InputAction
{
    public final  double          x;
    public final  double          y;

    public static
    CursorPosInputAction deserialize(DataInputStream dataInputStream) throws IOException
    {
        return new CursorPosInputAction(dataInputStream.readDouble(), dataInputStream.readDouble());
    }

    public
    CursorPosInputAction(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    @Override
    protected
    void serializeInternal(DataOutputStream dataOutputStream) throws IOException
    {
        dataOutputStream.writeDouble(this.x);
        dataOutputStream.writeDouble(this.y);
    }

    @Override
    protected
    InputActionType getType()
    {
        return InputActionType.CURSOR_POS;
    }

    @Override
    public
    void simulateAction()
    {
        GLFW.glfwSetCursorPos(InputManager.WINDOW_HANDLE, this.x, this.y);
        PycraftMouse.INSTANCE.onCursorPos(true, this.x, this.y);
    }
}
