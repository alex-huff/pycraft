package com.alexfh.pycraft.input.action.impl;

import com.alexfh.pycraft.input.action.InputAction;
import com.alexfh.pycraft.input.action.InputActionType;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public
class NOOPInputAction extends InputAction
{
    public static
    NOOPInputAction deserialize(DataInputStream dataInputStream) throws IOException
    {
        return new NOOPInputAction();
    }

    @Override
    protected
    void serializeInternal(DataOutputStream dataOutputStream) throws IOException
    {

    }

    @Override
    protected
    InputActionType getType()
    {
        return InputActionType.NOOP;
    }

    @Override
    public
    void simulateAction()
    {
    }
}
