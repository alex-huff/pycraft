package com.alexfh.pycraft.input.action.impl;

import com.alexfh.pycraft.input.PycraftKeyboard;
import com.alexfh.pycraft.input.action.InputAction;
import com.alexfh.pycraft.input.action.InputActionType;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public
class KeyInputAction extends InputAction
{
    public final int key;
    public final int scancode;
    public final int action;
    public final int modifiers;

    public static
    KeyInputAction deserialize(DataInputStream dataInputStream) throws IOException
    {
        return new KeyInputAction(dataInputStream.readInt(), dataInputStream.readInt(), dataInputStream.readInt(), dataInputStream.readInt());
    }

    public
    KeyInputAction(int key, int scancode, int action, int modifiers)
    {
        this.key       = key;
        this.scancode  = scancode;
        this.action    = action;
        this.modifiers = modifiers;
    }

    @Override
    protected
    void serializeInternal(DataOutputStream dataOutputStream) throws IOException
    {
        dataOutputStream.writeInt(this.key);
        dataOutputStream.writeInt(this.scancode);
        dataOutputStream.writeInt(this.action);
        dataOutputStream.writeInt(this.modifiers);
    }

    @Override
    protected
    InputActionType getType()
    {
        return InputActionType.KEY_INPUT;
    }

    @Override
    public
    void simulateAction()
    {
        PycraftKeyboard.INSTANCE.onKey(true, this.key, this.scancode, this.action, this.modifiers);
    }
}
