package com.alexfh.pycraft.input.action;

import com.alexfh.pycraft.input.action.impl.*;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public abstract
class InputAction
{
    private static final Map<InputActionType, InputActionDeserializer> deserializerMap = new HashMap<>();

    static
    {
        InputAction.deserializerMap.put(InputActionType.CHAR_INPUT, CharInputAction::deserialize);
        InputAction.deserializerMap.put(InputActionType.CURSOR_POS, CursorPosInputAction::deserialize);
        InputAction.deserializerMap.put(InputActionType.FILES_DROPPED, FilesDroppedInputAction::deserialize);
        InputAction.deserializerMap.put(InputActionType.KEY_INPUT, KeyInputAction::deserialize);
        InputAction.deserializerMap.put(InputActionType.MOUSE_BUTTON, MouseButtonInputAction::deserialize);
        InputAction.deserializerMap.put(InputActionType.MOUSE_SCROLL, MouseScrollInputAction::deserialize);
        InputAction.deserializerMap.put(InputActionType.NOOP, NOOPInputAction::deserialize);
    }

    @FunctionalInterface
    protected
    interface InputActionDeserializer
    {
        InputAction deserialize(DataInputStream dataInputStream) throws IOException;
    }

    public static
    InputAction deserialize(DataInputStream dataInputStream) throws IOException
    {
        InputActionType         inputActionType = InputActionType.deserialize(dataInputStream);
        InputActionDeserializer deserializer    = InputAction.deserializerMap.get(inputActionType);
        if (deserializer == null)
        {
            throw new IllegalStateException("tried to deserialize InputAction with no registered deserializer");
        }
        return deserializer.deserialize(dataInputStream);
    }

    public
    void serialize(DataOutputStream dataOutputStream) throws IOException
    {
        this.getType().serialize(dataOutputStream);
        this.serializeInternal(dataOutputStream);
    }

    protected abstract
    void serializeInternal(DataOutputStream dataOutputStream) throws IOException;

    protected abstract
    InputActionType getType();

    public abstract
    void simulateAction();
}
