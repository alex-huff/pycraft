package com.alexfh.pycraft.input;

import com.alexfh.pycraft.input.action.impl.CursorPosInputAction;
import com.alexfh.pycraft.input.action.impl.FilesDroppedInputAction;
import com.alexfh.pycraft.input.action.impl.MouseButtonInputAction;
import com.alexfh.pycraft.input.action.impl.MouseScrollInputAction;
import com.alexfh.pycraft.record.RecordingManager;
import net.minecraft.client.Mouse;
import org.lwjgl.glfw.GLFW;

import java.nio.file.Path;
import java.util.List;

public
class PycraftMouse
{
    public static final  PycraftMouse INSTANCE     = new PycraftMouse();
    private static final int          FIRST_BUTTON = GLFW.GLFW_MOUSE_BUTTON_1;
    private static final int          LAST_BUTTON  = GLFW.GLFW_MOUSE_BUTTON_LAST;
    private static final int          NUM_BUTTONS  = (PycraftMouse.LAST_BUTTON - PycraftMouse.FIRST_BUTTON) + 1;
    private final        boolean[]    pressed      = new boolean[PycraftMouse.NUM_BUTTONS];
    private              Mouse        mouse;

    public
    void setMouse(Mouse mouse)
    {
        this.mouse = mouse;
    }

    private
    void updatePressed(int button, int action)
    {
        this.pressed[button - PycraftMouse.FIRST_BUTTON] = action != GLFW.GLFW_RELEASE;
    }

    private
    boolean getPressed(int button)
    {
        return this.pressed[button - PycraftMouse.FIRST_BUTTON];
    }

    public
    void releaseAllInput()
    {
        for (int button = PycraftMouse.FIRST_BUTTON; button < PycraftMouse.LAST_BUTTON + 1; button++)
        {
            if (this.getPressed(button))
            {
                new MouseButtonInputAction(button, GLFW.GLFW_RELEASE, 0).simulateAction();
            }
        }
    }

    public
    void onCursorPos(boolean simulated, double x, double y)
    {
        boolean shouldCancel = RecordingManager.INSTANCE.onInput(simulated, new CursorPosInputAction(x, y));
        if (shouldCancel)
        {
            return;
        }
        this.mouse.onCursorPos(InputManager.WINDOW_HANDLE, x, y);
    }

    public
    void onMouseButton(boolean simulated, int button, int action, int mods)
    {
        boolean shouldCancel
            = RecordingManager.INSTANCE.onInput(simulated, new MouseButtonInputAction(button, action, mods));
        if (shouldCancel)
        {
            return;
        }
        this.updatePressed(button, action);
        this.mouse.onMouseButton(InputManager.WINDOW_HANDLE, button, action, mods);
    }

    public
    void onMouseScroll(boolean simulated, double horizontal, double vertical)
    {
        boolean shouldCancel
            = RecordingManager.INSTANCE.onInput(simulated, new MouseScrollInputAction(horizontal, vertical));
        if (shouldCancel)
        {
            return;
        }
        this.mouse.onMouseScroll(InputManager.WINDOW_HANDLE, horizontal, vertical);
    }

    public
    void onFilesDropped(boolean simulated, List<Path> paths)
    {
        boolean shouldCancel = RecordingManager.INSTANCE.onInput(simulated, new FilesDroppedInputAction(paths));
        if (shouldCancel)
        {
            return;
        }
        this.mouse.onFilesDropped(InputManager.WINDOW_HANDLE, paths);
    }
}
