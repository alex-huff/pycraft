package com.alexfh.pycraft.input.action.impl;

import com.alexfh.pycraft.input.PycraftMouse;
import com.alexfh.pycraft.input.action.InputAction;
import com.alexfh.pycraft.input.action.InputActionType;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public
class MouseScrollInputAction extends InputAction
{
    public final double horizontal;
    public final double vertical;

    public static
    MouseScrollInputAction deserialize(DataInputStream dataInputStream) throws IOException
    {
        return new MouseScrollInputAction(dataInputStream.readDouble(), dataInputStream.readDouble());
    }

    public
    MouseScrollInputAction(double horizontal, double vertical)
    {
        this.horizontal = horizontal;
        this.vertical   = vertical;
    }

    @Override
    protected
    void serializeInternal(DataOutputStream dataOutputStream) throws IOException
    {
        dataOutputStream.writeDouble(this.horizontal);
        dataOutputStream.writeDouble(this.vertical);
    }

    @Override
    protected
    InputActionType getType()
    {
        return InputActionType.MOUSE_SCROLL;
    }

    @Override
    public
    void simulateAction()
    {
        PycraftMouse.INSTANCE.onMouseScroll(true, this.horizontal, this.vertical);
    }
}
