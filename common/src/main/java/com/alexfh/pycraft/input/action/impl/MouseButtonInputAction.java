package com.alexfh.pycraft.input.action.impl;

import com.alexfh.pycraft.input.PycraftMouse;
import com.alexfh.pycraft.input.action.InputAction;
import com.alexfh.pycraft.input.action.InputActionType;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public
class MouseButtonInputAction extends InputAction
{
    public final int button;
    public final int action;
    public final int mods;

    public static
    MouseButtonInputAction deserialize(DataInputStream dataInputStream) throws IOException
    {
        return new MouseButtonInputAction(dataInputStream.readInt(), dataInputStream.readInt(), dataInputStream.readInt());
    }

    public
    MouseButtonInputAction(int button, int action, int mods)
    {
        this.button = button;
        this.action = action;
        this.mods   = mods;
    }

    @Override
    protected
    void serializeInternal(DataOutputStream dataOutputStream) throws IOException
    {
        dataOutputStream.writeInt(this.button);
        dataOutputStream.writeInt(this.action);
        dataOutputStream.writeInt(this.mods);
    }

    @Override
    protected
    InputActionType getType()
    {
        return InputActionType.MOUSE_BUTTON;
    }

    @Override
    public
    void simulateAction()
    {
        PycraftMouse.INSTANCE.onMouseButton(true, this.button, this.action, this.mods);
    }
}
