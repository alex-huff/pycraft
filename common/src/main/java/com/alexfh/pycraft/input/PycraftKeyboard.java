package com.alexfh.pycraft.input;

import com.alexfh.pycraft.input.action.impl.CharInputAction;
import com.alexfh.pycraft.input.action.impl.KeyInputAction;
import com.alexfh.pycraft.record.RecordingManager;
import net.minecraft.client.Keyboard;
import org.lwjgl.glfw.GLFW;

public
class PycraftKeyboard
{
    public static final  PycraftKeyboard INSTANCE  = new PycraftKeyboard();
    private static final int             FIRST_KEY = GLFW.GLFW_KEY_SPACE;
    private static final int             LAST_KEY  = GLFW.GLFW_KEY_LAST;
    private static final int             NUM_KEYS  = (PycraftKeyboard.LAST_KEY - PycraftKeyboard.FIRST_KEY) + 1;
    private final        boolean[]       pressed   = new boolean[PycraftKeyboard.NUM_KEYS];
    private              Keyboard        keyboard;

    public
    void setKeyboard(Keyboard keyboard)
    {
        this.keyboard = keyboard;
    }

    private
    void updatePressed(int key, int action)
    {
        if (action == GLFW.GLFW_REPEAT)
        {
            return;
        }
        this.pressed[key - PycraftKeyboard.FIRST_KEY] = action != GLFW.GLFW_RELEASE;
    }

    public
    boolean getPressed(int key)
    {
        return this.pressed[key - PycraftKeyboard.FIRST_KEY];
    }

    public
    void releaseAllInput()
    {
        for (int key = PycraftKeyboard.FIRST_KEY; key < PycraftKeyboard.LAST_KEY + 1; key++)
        {
            if (this.getPressed(key))
            {
                new KeyInputAction(key, GLFW.glfwGetKeyScancode(key), GLFW.GLFW_RELEASE, 0).simulateAction();
            }
        }
    }

    public
    void onKey(boolean simulated, int key, int scancode, int action, int modifiers)
    {
        boolean shouldCancel
            = RecordingManager.INSTANCE.onInput(simulated, new KeyInputAction(key, scancode, action, modifiers));
        if (shouldCancel)
        {
            return;
        }
        this.updatePressed(key, action);
        this.keyboard.onKey(InputManager.WINDOW_HANDLE, key, scancode, action, modifiers);
    }

    public
    void onChar(boolean simulated, int codepoint, int modifiers)
    {
        boolean shouldCancel = RecordingManager.INSTANCE.onInput(simulated, new CharInputAction(codepoint, modifiers));
        if (shouldCancel)
        {
            return;
        }
        this.keyboard.onChar(InputManager.WINDOW_HANDLE, codepoint, modifiers);
    }
}
