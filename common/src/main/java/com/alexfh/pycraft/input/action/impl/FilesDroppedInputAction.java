package com.alexfh.pycraft.input.action.impl;

import com.alexfh.pycraft.input.PycraftMouse;
import com.alexfh.pycraft.input.action.InputAction;
import com.alexfh.pycraft.input.action.InputActionType;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public
class FilesDroppedInputAction extends InputAction
{
    public final List<Path> paths;

    public static
    FilesDroppedInputAction deserialize(DataInputStream dataInputStream) throws IOException
    {
        int        numPaths = dataInputStream.readInt();
        List<Path> paths    = new ArrayList<>(numPaths);
        for (int i = 0; i < numPaths; i++)
        {
            paths.add(Path.of(dataInputStream.readUTF()));
        }
        return new FilesDroppedInputAction(paths);
    }

    public
    FilesDroppedInputAction(List<Path> paths)
    {
        this.paths = paths;
    }

    @Override
    protected
    void serializeInternal(DataOutputStream dataOutputStream) throws IOException
    {
        dataOutputStream.writeInt(this.paths.size());
        for (Path path : this.paths)
        {
            dataOutputStream.writeUTF(path.toString());
        }
    }

    @Override
    protected
    InputActionType getType()
    {
        return InputActionType.FILES_DROPPED;
    }

    @Override
    public
    void simulateAction()
    {
        PycraftMouse.INSTANCE.onFilesDropped(true, this.paths);
    }
}
