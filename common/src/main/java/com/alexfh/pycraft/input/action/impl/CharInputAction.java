package com.alexfh.pycraft.input.action.impl;

import com.alexfh.pycraft.input.PycraftKeyboard;
import com.alexfh.pycraft.input.action.InputAction;
import com.alexfh.pycraft.input.action.InputActionType;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public
class CharInputAction extends InputAction
{
    public final int codepoint;
    public final int modifiers;

    public static
    CharInputAction deserialize(DataInputStream dataInputStream) throws IOException
    {
        return new CharInputAction(dataInputStream.readInt(), dataInputStream.readInt());
    }

    public
    CharInputAction(int codePoint, int modifiers)
    {
        this.codepoint = codePoint;
        this.modifiers = modifiers;
    }

    @Override
    protected
    void serializeInternal(DataOutputStream dataOutputStream) throws IOException
    {
        dataOutputStream.writeInt(this.codepoint);
        dataOutputStream.writeInt(this.modifiers);
    }

    @Override
    protected
    InputActionType getType()
    {
        return InputActionType.CHAR_INPUT;
    }

    @Override
    public
    void simulateAction()
    {
        PycraftKeyboard.INSTANCE.onChar(true, this.codepoint, this.modifiers);
    }
}
