package com.alexfh.pycraft.input.action;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public
enum InputActionType
{
    KEY_INPUT, CHAR_INPUT, CURSOR_POS, MOUSE_BUTTON, MOUSE_SCROLL, FILES_DROPPED, NOOP;

    private static final InputActionType[] values = InputActionType.values();

    public
    void serialize(DataOutputStream dataOutputStream) throws IOException
    {
        dataOutputStream.writeByte(this.ordinal());
    }

    public static
    InputActionType deserialize(DataInputStream dataInputStream) throws IOException
    {
        return InputActionType.values[dataInputStream.readByte()];
    }
}
