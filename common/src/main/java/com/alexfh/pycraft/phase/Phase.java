package com.alexfh.pycraft.phase;

import java.util.HashMap;
import java.util.Map;

public
enum Phase
{

    NONE, TICK_HEAD, RENDER_HEAD, INIT;

    public static final Map<String, Phase> nameMap;

    static
    {
        nameMap = new HashMap<>();
        nameMap.put("none", NONE);
        nameMap.put("tick", TICK_HEAD);
        nameMap.put("render", RENDER_HEAD);
        nameMap.put("init", INIT);
    }
}
