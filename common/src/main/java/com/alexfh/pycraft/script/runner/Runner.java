package com.alexfh.pycraft.script.runner;

import com.alexfh.pycraft.phase.Phase;

public
interface Runner
{
    boolean launch();

    void continueIfWaitingFor(Phase phase);

    boolean isCompleted();
}
