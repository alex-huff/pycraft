package com.alexfh.pycraft.script;

import com.alexfh.pycraft.phase.Phase;
import com.alexfh.pycraft.script.runner.Runner;
import com.alexfh.pycraft.script.runner.impl.JepRunner;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public
class ScriptManager
{
    public static final ScriptManager INSTANCE          = new ScriptManager();
    public              List<Runner>  inProgressRunners = new ArrayList<>();

    public
    void runScript(Path scriptPath)
    {
        this.runScript(scriptPath, new String[0]);
    }

    public
    void runScript(Path scriptPath, String[] arguments)
    {
        Runner  runner    = new JepRunner(scriptPath, arguments);
        boolean completed = runner.launch();
        if (!completed)
        {
            this.inProgressRunners.add(runner);
        }
    }

    public
    void continueScriptsWaitingForPhase(Phase phase)
    {
        this.inProgressRunners.removeIf(Runner::isCompleted);
        this.inProgressRunners.forEach(runner -> runner.continueIfWaitingFor(phase));
    }
}
