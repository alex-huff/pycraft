package com.alexfh.pycraft.script.runner.impl;

import jep.Interpreter;
import jep.JepException;
import jep.SharedInterpreter;

import java.nio.file.Path;

public
class JepRunner extends AsyncRunnerBase
{
    public
    JepRunner(Path scriptPath, String[] arguments)
    {
        super(scriptPath, arguments);
    }

    @Override
    protected
    void runScript()
    {
        try (Interpreter interpreter = new SharedInterpreter())
        {
            interpreter.set("mc", this.new MinecraftController());
            interpreter.set("controller", this.new AsyncController());
            interpreter.set("args", this.arguments);
            interpreter.runScript(this.scriptPath.toString());
        }
        catch (JepException jepException)
        {
            jepException.printStackTrace();
        }
    }
}
