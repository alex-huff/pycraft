package com.alexfh.pycraft.script.runner.impl;

import com.alexfh.pycraft.direction.PitchYawInterpolater;
import com.alexfh.pycraft.executor.PycraftExecutorService;
import com.alexfh.pycraft.io.format.pcr.PCRFormat;
import com.alexfh.pycraft.phase.Phase;
import com.alexfh.pycraft.record.Recording;
import com.alexfh.pycraft.record.RecordingException;
import com.alexfh.pycraft.record.RecordingManager;
import com.alexfh.pycraft.util.PycraftUtil;

import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract
class AsyncRunnerBase extends RunnerBase
{

    protected
    class AsyncController
    {
        public
        void yieldTill(String... phases)
        {

            this.yieldTill(1, phases);
        }

        public
        void yieldTill(int cycles, String... phases)
        {
            Phase[] mappedPhases = new Phase[phases.length];
            for (int i = 0; i < phases.length; i++)
            {
                String phaseName = phases[i];
                if (!Phase.nameMap.containsKey(phaseName))
                {
                    throw new IllegalArgumentException(phaseName + " is not a valid phase");
                }
                mappedPhases[i] = Phase.nameMap.get(phaseName);
            }
            AsyncRunnerBase.this.yield(cycles, mappedPhases);
        }
    }

    protected
    class MinecraftController extends RunnerBase.MinecraftController
    {

        @Override
        public
        void turnHead(PitchYawInterpolater pitchYawInterpolater)
        {
            pitchYawInterpolater.apply(0, 0);
            this.lookAt(pitchYawInterpolater.getPitch(), pitchYawInterpolater.getYaw());
            AsyncRunnerBase runner = AsyncRunnerBase.this;
            runner.pushPhasesToYieldFor(Phase.TICK_HEAD, Phase.RENDER_HEAD);
            int currentTick = 0;
            while (currentTick < pitchYawInterpolater.getTotalTicks())
            {
                runner.yield();
                float tickDelta = 0;
                switch (runner.phase)
                {
                    case TICK_HEAD -> currentTick++;
                    case RENDER_HEAD -> tickDelta = this.minecraftClient.getTickDelta();
                }
                pitchYawInterpolater.apply(currentTick, tickDelta);
                this.lookAt(pitchYawInterpolater.getPitch(), pitchYawInterpolater.getYaw());
            }
            runner.popPhasesToYieldFor();
        }

        @Override
        public
        Recording loadRecording(String filePathString)
        {
            AsyncRunnerBase runner   = AsyncRunnerBase.this;
            Path            filePath = Path.of(filePathString);
            if (!filePath.isAbsolute())
            {
                filePath = PycraftUtil.getRecordingDirectory().resolve(filePath);
            }
            final Path                   finalFilePath   = filePath;
            CompletableFuture<Recording> recordingFuture = new CompletableFuture<>();
            PycraftExecutorService.INSTANCE.submit(() ->
            {
                try
                {
                    recordingFuture.complete(PCRFormat.readPCRFile(finalFilePath));
                }
                catch (Throwable t)
                {
                    recordingFuture.completeExceptionally(t);
                }
                finally
                {
                    this.minecraftClient.execute(runner::continueScript);
                }
            });
            runner.pushPhasesToYieldFor(Phase.NONE);
            runner.yield();
            runner.popPhasesToYieldFor();
            return recordingFuture.join();
        }

        @Override
        public
        boolean playbackRecording(Recording recording)
        {
            AsyncRunnerBase runner            = AsyncRunnerBase.this;
            AtomicBoolean   playbackSucceeded = new AtomicBoolean();
            try
            {
                RecordingManager.INSTANCE.playbackRecording(recording, (succeeded) ->
                {
                    playbackSucceeded.set(succeeded);
                    runner.continueScript();
                });
            }
            catch (RecordingException recordingException)
            {
                recordingException.printStackTrace();
                return false;
            }
            runner.pushPhasesToYieldFor(Phase.NONE);
            runner.yield();
            runner.popPhasesToYieldFor();
            return playbackSucceeded.get();
        }
    }

    protected final Path              scriptPath;
    protected final String[]          arguments;
    private final   Stack<Set<Phase>> yieldingFor         = new Stack<>();
    private final   Lock              runnerLock          = new ReentrantLock();
    private final   Condition         yieldingOrCompleted = this.runnerLock.newCondition();
    private final   Condition         scriptContinued     = this.runnerLock.newCondition();
    private         boolean           isYielding          = false;
    private         boolean           isCompleted         = false;
    private         Phase             phase               = Phase.INIT;

    protected
    AsyncRunnerBase(Path scriptPath, String[] arguments)
    {
        this.scriptPath = scriptPath;
        this.arguments  = arguments;
    }

    @Override
    public
    boolean launch()
    {
        PycraftExecutorService.INSTANCE.submit(this::runScriptWrapper);
        return this.waitForYieldOrCompletion();
    }

    private
    void continueScript()
    {
        this.continueScript(Phase.NONE);
    }

    private
    void continueScript(Phase phase)
    {
        this.phase = phase;
        this.signalScriptToContinue();
        this.waitForYieldOrCompletion();
    }

    @Override
    public
    void continueIfWaitingFor(Phase phase)
    {
        if (this.yieldingFor.empty() || !this.yieldingFor.peek().contains(phase))
        {
            return;
        }
        this.continueScript(phase);
    }

    @Override
    public
    boolean isCompleted()
    {
        return this.isCompleted;
    }

    private
    void signalScriptToContinue()
    {
        try
        {
            this.runnerLock.lock();
            this.isYielding = false;
            this.scriptContinued.signal();
        }
        finally
        {
            this.runnerLock.unlock();
        }
    }

    private
    boolean waitForYieldOrCompletion()
    {
        try
        {
            this.runnerLock.lock();
            while (!this.isYielding && !this.isCompleted)
            {
                this.yieldingOrCompleted.await();
            }
            return this.isCompleted;
        }
        catch (InterruptedException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            this.runnerLock.unlock();
        }
    }

    private
    void pushPhasesToYieldFor(Phase... phases)
    {
        this.yieldingFor.push(new HashSet<>(List.of(phases)));
    }

    private
    void popPhasesToYieldFor()
    {
        this.yieldingFor.pop();
    }

    private
    void yield(Phase... phases)
    {
        this.yield(1, phases);
    }


    private
    void yield(int cycles, Phase... phases)
    {
        this.pushPhasesToYieldFor(phases);
        this.yield(cycles);
        this.popPhasesToYieldFor();
    }

    private
    void yield()
    {
        this.yield(1);
    }

    private
    void yield(int cycles)
    {
        if (cycles == 0)
        {
            return;
        }
        try
        {
            this.runnerLock.lock();
            for (int i = 0; i < cycles; i++)
            {
                this.isYielding = true;
                this.yieldingOrCompleted.signal();
                while (this.isYielding)
                {
                    this.scriptContinued.await();
                }
            }
        }
        catch (InterruptedException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            this.runnerLock.unlock();
        }
    }

    private
    void runScriptWrapper()
    {
        this.runScript();
        try
        {
            this.runnerLock.lock();
            this.isCompleted = true;
            this.yieldingOrCompleted.signal();
        }
        finally
        {
            this.runnerLock.unlock();
        }
    }

    protected abstract
    void runScript();
}
