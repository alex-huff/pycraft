package com.alexfh.pycraft.script.runner.impl;

import com.alexfh.pycraft.direction.PitchYawInterpolater;
import com.alexfh.pycraft.direction.YawDirection;
import com.alexfh.pycraft.direction.impl.WrappedPitchYawInterpolater;
import com.alexfh.pycraft.record.Recording;
import com.alexfh.pycraft.script.runner.Runner;
import com.alexfh.pycraft.util.MinecraftUtil;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;

public abstract
class RunnerBase implements Runner
{
    protected abstract
    class MinecraftController
    {
        protected final MinecraftClient minecraftClient  = MinecraftClient.getInstance();
        private         boolean         simulateInputs   = false;
        private         boolean         relativeMovement = true;

        public
        void setSimulateInputs(boolean simulateInputs)
        {
            this.simulateInputs = simulateInputs;
        }

        public
        void setRelativeMovement(boolean relativeMovement)
        {
            this.relativeMovement = relativeMovement;
        }

        private
        ClientPlayerEntity getPlayer()
        {
            ClientPlayerEntity player = this.minecraftClient.player;
            if (player == null)
            {
                throw new IllegalStateException("tried to get player, but there is no active player");
            }
            return player;
        }

        public
        void lookAt(float pitch, float yaw)
        {
            ClientPlayerEntity clientPlayerEntity = this.getPlayer();
            boolean            absolute           = !this.relativeMovement;
            if (this.simulateInputs)
            {
                MinecraftUtil.simulateMouseLookAt(clientPlayerEntity, pitch, yaw, absolute);
            }
            else
            {
                MinecraftUtil.lookAt(clientPlayerEntity, pitch, yaw, absolute);
            }
        }

        public
        float[] getHeadDirection()
        {
            ClientPlayerEntity player = this.getPlayer();
            return new float[]{ player.getPitch(), player.getYaw() };
        }

        public
        void turnHead(int ticks, float endPitch, float endYaw)
        {
            float[] headDirection = this.getHeadDirection();
            float   startPitch    = headDirection[0];
            float   startYaw      = headDirection[1];
            this.turnHead(ticks, startPitch, startYaw, endPitch, endYaw);
        }

        public
        void turnHead(int ticks, float endPitch, float endYaw, YawDirection yawDirection)
        {
            float[] headDirection = this.getHeadDirection();
            float   startPitch    = headDirection[0];
            float   startYaw      = headDirection[1];
            this.turnHead(ticks, startPitch, startYaw, endPitch, endYaw, yawDirection);
        }

        public
        void turnHead(int ticks, float startPitch, float startYaw, float endPitch, float endYaw)
        {
            this.turnHead(ticks, startPitch, startYaw, endPitch, endYaw, YawDirection.CLOSEST);
        }

        public
        void turnHead(int ticks, float startPitch, float startYaw, float endPitch, float endYaw,
                      YawDirection yawDirection)
        {
            PitchYawInterpolater pitchYawInterpolater
                = new WrappedPitchYawInterpolater(ticks, startPitch, startYaw, endPitch, endYaw, yawDirection);
            this.turnHead(pitchYawInterpolater);
        }

        public abstract
        void turnHead(PitchYawInterpolater pitchYawInterpolater);

        public abstract
        Recording loadRecording(String filePath);

        public abstract
        boolean playbackRecording(Recording recording);
    }
}
