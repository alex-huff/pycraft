package com.alexfh.pycraft.util;

import com.alexfh.pycraft.ExpectPlatform;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public
class PycraftUtil
{

    private static final Path DIRECTORY            = ExpectPlatform.getGameDirectory().resolve("pycraft");
    private static final Path SCRIPTS_DIRECTORY    = PycraftUtil.DIRECTORY.resolve("scripts");
    private static final Path RECORDINGS_DIRECTORY = PycraftUtil.DIRECTORY.resolve("recordings");

    public static
    void verifyPycraftDirectoriesExist()
    {
        try
        {
            Files.createDirectories(PycraftUtil.SCRIPTS_DIRECTORY);
            Files.createDirectories(PycraftUtil.RECORDINGS_DIRECTORY);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public static
    Path getPycraftDirectory()
    {
        return PycraftUtil.DIRECTORY;
    }

    public static
    Path getScriptsDirectory()
    {
        return PycraftUtil.SCRIPTS_DIRECTORY;
    }

    public static
    Path getRecordingDirectory()
    {
        return PycraftUtil.RECORDINGS_DIRECTORY;
    }
}
