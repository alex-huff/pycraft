package com.alexfh.pycraft.util;

import com.alexfh.pycraft.input.action.impl.CursorPosInputAction;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.Mouse;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

public
class MinecraftUtil
{
    public static
    float clampPitch(float pitch)
    {
        return MathHelper.clamp(pitch, -90f, 90f);
    }

    public static
    float closestYaw(float from, float target)
    {
        int x = Math.round((from - target) / 360);
        return Math.fma(360, x, target);
    }

    public static
    void lookAt(ClientPlayerEntity player, float prevPitch, float prevYaw, float pitch, float yaw)
    {
        /*
        There is some difference in behavior between using this method to change
        the player's direction and actually using the mouse. More can be done to
        make the behavior more similar. See Mouse::onCursorPos.
         */
        player.setPitch(MinecraftUtil.clampPitch(pitch));
        player.setYaw(yaw);
        player.prevPitch = MinecraftUtil.clampPitch(prevPitch);
        player.prevYaw   = prevYaw;
        Entity vehicle = player.getVehicle();
        if (vehicle != null)
        {
            vehicle.onPassengerLookAround(player);
        }
    }

    public static
    void lookAt(ClientPlayerEntity player, float pitch, float yaw, boolean absolute)
    {
        if (!absolute)
        {
            yaw = MinecraftUtil.closestYaw(player.getYaw(), yaw);
        }
        float dp        = pitch - player.getPitch();
        float dy        = yaw - player.getYaw();
        float prevPitch = player.prevPitch + dp;
        float prevYaw   = player.prevYaw + dy;
        MinecraftUtil.lookAt(player, prevPitch, prevYaw, pitch, yaw);
    }

    public static
    void simulateMouseLookAt(ClientPlayerEntity player, float pitch, float yaw, boolean absolute)
    {
        if (!absolute)
        {
            yaw = MinecraftUtil.closestYaw(player.getYaw(), yaw);
        }
        MinecraftClient minecraftClient = MinecraftClient.getInstance();
        Mouse           mouse           = minecraftClient.mouse;
        double          f               = minecraftClient.options.getMouseSensitivity().getValue() * (double) 0.6f +
                                          (double) 0.2f;
        double          g               = f * f * f;
        double          h               = g * 8.0;
        double          i;
        if (minecraftClient.options.smoothCameraEnabled)
        {
            throw new IllegalStateException("simulateMouseLookAt does not support smooth camera");
        }
        else if (minecraftClient.options.getPerspective().isFirstPerson() && player.isUsingSpyglass())
        {
            i = g;
        }
        else
        {
            i = h;
        }
        double m = minecraftClient.options.getInvertYMouse().getValue() ? -1 : 1;
        /*
        k = cdx * i;
        l = cdy * i;
        dp = l * m * .15f;
        dy = k * .15f;
        dp = cdy * i * m * .15f
        dy = cdx * i * .15f
        cdy = dp / (i * m * .15f)
        cdx = dy / (i * .15f)
        cdx = nx - x
        cdy = ny - y
        nx = cdx + x
        ny = cdy + y
         */
        double targetDeltaPitch   = pitch - player.getPitch();
        double targetDeltaYaw     = yaw - player.getYaw();
        double targetCursorDeltaX = targetDeltaYaw / (i * .15f);
        double targetCursorDeltaY = targetDeltaPitch / (i * m * .15f);
        double newCursorX         = targetCursorDeltaX + mouse.x;
        double newCursorY         = targetCursorDeltaY + mouse.y;
        new CursorPosInputAction(newCursorX, newCursorY).simulateAction();
    }

}
