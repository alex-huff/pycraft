package com.alexfh.pycraft.util;

public
class MathUtil
{
    public static
    double lerp(double start, double finish, double percent)
    {
        return (1 - percent) * start + percent * finish;
    }
}
