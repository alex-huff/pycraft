package com.alexfh.pycraft.fabric;

import com.alexfh.pycraft.Pycraft;
import net.fabricmc.api.ModInitializer;

public
class PycraftFabric implements ModInitializer
{
    @Override
    public
    void onInitialize()
    {
        Pycraft.init();
    }
}
