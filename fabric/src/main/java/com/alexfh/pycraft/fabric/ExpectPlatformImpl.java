package com.alexfh.pycraft.fabric;

import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.impl.launch.knot.Knot;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

public
class ExpectPlatformImpl
{
    public static
    Path getGameDirectory()
    {
        return FabricLoader.getInstance().getGameDir();
    }

    public static
    List<Path> getClassPath()
    {
        ClassLoader                  currentClassLoader   = Knot.getLauncher().getTargetClassLoader();
        Class<? extends ClassLoader> knotClassLoaderClass = currentClassLoader.getClass();
        URLClassLoader               urlClassLoader;
        try
        {
            Field urlClassLoaderField = Stream.of(knotClassLoaderClass.getDeclaredFields())
                .filter(field -> URLClassLoader.class.isAssignableFrom(field.getType())).findFirst().orElseThrow();
            urlClassLoaderField.setAccessible(true);
            urlClassLoader = (URLClassLoader) urlClassLoaderField.get(currentClassLoader);
        }
        catch (IllegalAccessException exception)
        {
            throw new RuntimeException(exception);
        }
        return Stream.of(urlClassLoader.getURLs()).map(URL::getPath).map(File::new).map(File::toPath).toList();
    }
}
