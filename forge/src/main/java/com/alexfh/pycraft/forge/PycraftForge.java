package com.alexfh.pycraft.forge;

import com.alexfh.pycraft.Pycraft;
import dev.architectury.platform.forge.EventBuses;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(Pycraft.MOD_ID)
public
class PycraftForge
{
    public
    PycraftForge()
    {
        EventBuses.registerModEventBus(Pycraft.MOD_ID, FMLJavaModLoadingContext.get().getModEventBus());
        Pycraft.init();
    }
}
