package com.alexfh.pycraft.forge;

import net.minecraftforge.fml.loading.FMLPaths;
import org.apache.commons.io.filefilter.SuffixFileFilter;

import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public
class ExpectPlatformImpl
{
    public static
    Path getGameDirectory()
    {
        return FMLPaths.GAMEDIR.get();
    }

    public static
    List<Path> getClassPath()
    {
        return Stream.concat(Stream.of(System.getProperty("java.class.path")
                .split(System.getProperty("path.separator")))
            .map(File::new), Stream.of(Objects.requireNonNull(FMLPaths.MODSDIR.get().toFile()
            .listFiles((FilenameFilter) new SuffixFileFilter(".jar"))))).map(File::toPath).toList();
    }
}
